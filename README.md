# makemy-trip-clone-script
<div dir="ltr" style="text-align: left;" trbidi="on">
<div class="short-description" style="background-color: white; border-bottom: 1px solid rgb(243, 232, 229); box-sizing: border-box; color: #666666; font-family: Arial; font-size: 12px; margin-bottom: 27px; padding-bottom: 27px;">
<h5 style="box-sizing: border-box; color: #222222; font-family: Poppins; font-size: 14px; line-height: 18px; margin-bottom: 10px; margin-top: 0px;">
Quick overview:</h5>
<div class="std" itemprop="description" style="box-sizing: border-box; font-size: 13px; line-height: 2em;">
<b><em style="box-sizing: border-box;">This document is prepared by DOD IT Solutions, to give you an idea of how our&nbsp;<span style="box-sizing: border-box;"><a href="http://phpreadymadescripts.com/shop/make-my-trip-hotel-clone-script.html">MAKE MY TRIP</a></span>&nbsp;<span style="box-sizing: border-box;">HOTEL CLONE&nbsp;</span><span style="box-sizing: border-box;">SCRIPT</span>&nbsp;features would be.&nbsp;</em>&nbsp;Our&nbsp;<span style="box-sizing: border-box;"><em style="box-sizing: border-box;">Hotel Clone Script</em></span>&nbsp;is completely in built and has automated modules which are specific for online travel business. Online web based benefits of hotel room reservation process, create more sale leads and track customers, and most of all, it helps in maintaining your repeated customers info. You can analyze reservation trend details and work towards your future needs.</b></div>
</div>
<form action="http://phpreadymadescripts.com/checkout/cart/add/uenc/aHR0cDovL3BocHJlYWR5bWFkZXNjcmlwdHMuY29tL3Nob3AvbWFrZS1teS10cmlwLWhvdGVsLWNsb25lLXNjcmlwdC5odG1s/product/304/form_key/gQtwHN5sIxM077vP/" id="product_addtocart_form" method="post" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial;">
<div class="add-to-box" style="box-sizing: border-box;">
<strong style="box-sizing: border-box; font-size: 12.996px;"><em style="box-sizing: border-box;">UNIQUE FEATURES:</em></strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Room/Inventory availability</li>
<li style="box-sizing: border-box;">Real Time Pricing</li>
<li style="box-sizing: border-box;">Real Time Booking/Reservation</li>
<li style="box-sizing: border-box;">Online Cancellation/Refund</li>
<li style="box-sizing: border-box;">Booking Amendments</li>
<li style="box-sizing: border-box;">Travel Advisory Services</li>
<li style="box-sizing: border-box;">Destination Management</li>
<li style="box-sizing: border-box;">White Label Solutions</li>
<li style="box-sizing: border-box;">Affiliate Model</li>
<li style="box-sizing: border-box;">Multiple Payment Options</li>
<li style="box-sizing: border-box;">Easy to manage Property Information</li>
<li style="box-sizing: border-box;">Room Type , Inventory</li>
<li style="box-sizing: border-box;">Room Rate Amenities</li>
<li style="box-sizing: border-box;">Seasonal Rate and Other Charges</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;"><em style="box-sizing: border-box;">COMMON&nbsp; FEATURES</em></strong><span style="font-size: 12.996px;">&nbsp;</span><strong style="box-sizing: border-box; font-size: 12.996px;">SEARCH OPTIONS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">User can search by entering location, check-in and check-out date along with the number of rooms and as well the number of adults/children whose going to reside.</li>
<li style="box-sizing: border-box;">Map search is also available.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">FILTER OPTIONS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">User can check by selecting the stars (either 3 star hotels or 5 star hotels...etc)</li>
<li style="box-sizing: border-box;">Check by filtering the pay scale ranging from the least amount to the highest.</li>
<li style="box-sizing: border-box;">Can check by selecting a particular location.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">H</strong><strong style="box-sizing: border-box; font-size: 12.996px;">OTEL&nbsp; INFO</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">User can view the complete address of the selected hotel.</li>
<li style="box-sizing: border-box;">Can view images and hotel amenities.</li>
<li style="box-sizing: border-box;">Choose the particular room that you wish (Singe/Double/Luxury/Deluxe...etc).</li>
<li style="box-sizing: border-box;">Access to API Hotels and Offer Hotels.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;"><em style="box-sizing: border-box;">MAKE MY TRIP HOTEL ADMIN PANEL</em></strong><span style="font-size: 12.996px;">&nbsp;</span><strong style="box-sizing: border-box; font-size: 12.996px;">LOGIN</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Log in using desired username and password&nbsp; (provided by admin)</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">PROFILE</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">View the Agent details</li>
<li style="box-sizing: border-box;">Edit the agent details, block and unblock the agent record</li>
<li style="box-sizing: border-box;">Change the login Password</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">HOTEL MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Add the trip details</li>
<li style="box-sizing: border-box;">Searched by trip details using by&nbsp; hotel type, from the city, and status</li>
<li style="box-sizing: border-box;">List the &nbsp;trip details&nbsp; using on load Ajax functions</li>
<li style="box-sizing: border-box;">Manage the trip details (Edit, delete, block and unblock, pagination)</li>
<li style="box-sizing: border-box;">View and manage details</li>
<li style="box-sizing: border-box;">View the particular hotel details</li>
<li style="box-sizing: border-box;">structures designed by drag and drop method</li>
<li style="box-sizing: border-box;">Can add/delete hotel details.</li>
<li style="box-sizing: border-box;">Manage bookings and tickets list.</li>
<li style="box-sizing: border-box;">Manage image uploads.</li>
<li style="box-sizing: border-box;">Manage rooms.</li>
<li style="box-sizing: border-box;">Manage user info.</li>
<li style="box-sizing: border-box;">Maintain payment gateway and user wallet.</li>
<li style="box-sizing: border-box;">Manage day’s offers.</li>
<li style="box-sizing: border-box;">Complete end-to-end access.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">HOTEL IMAGES AND VIDEOS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Add the hotel images</li>
<li style="box-sizing: border-box;">Manage the hotel images(edit, status,delete)</li>
<li style="box-sizing: border-box;">Upload the hotel videos</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">PASSENGER MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">List the booked traveler details</li>
<li style="box-sizing: border-box;">View the particular guest details</li>
<li style="box-sizing: border-box;">View the booking details</li>
<li style="box-sizing: border-box;">View the particular count and view the hotel details</li>
<li style="box-sizing: border-box;">Searched by hotel details using ticket no , booked date, user type, from city , to city</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">SEAT MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Search and then list the seat details</li>
<li style="box-sizing: border-box;">Searched by seat details using date</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">TRAVELER MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">List the all details</li>
<li style="box-sizing: border-box;">View the particular traveler details</li>
<li style="box-sizing: border-box;">Filtered by traveler details using date,from to city</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">CANCEL</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">List all traveler</li>
<li style="box-sizing: border-box;">View the particular details</li>
<li style="box-sizing: border-box;">Filtered by tickets using ticket no, cancel date, travel date</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">PAYMENT MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">List the hotel details</li>
<li style="box-sizing: border-box;">View the particular payment transaction details</li>
<li style="box-sizing: border-box;">Filtered by trip details using hotel type, from city, to city , date</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">CANCELLATION POLICIES</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Add the refund status</li>
<li style="box-sizing: border-box;">Manage the details(edit,status,delete)</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">SMS LOG DETAILS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">List the sms details</li>
<li style="box-sizing: border-box;">Manage the details(delete)</li>
<li style="box-sizing: border-box;">Search sms details by bus and day,month,year</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">EMAIL LOG DETAILS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">List the email details</li>
<li style="box-sizing: border-box;">Manage the details(delete)</li>
<li style="box-sizing: border-box;">Search details details by bus and day,month,year</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">MANAGE BANNERS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Banners to pop-up with offers would be generated.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">MANAGE MARQUEE TEXT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Texts as flash notes either at the top or bottom of the page could be generated.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">COUPON</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Day-to-day offer coupon codes would be generated.</li>
</ul>
<span style="font-size: 12.996px;">[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column width="1/3"][vc_column_text]</span><strong style="box-sizing: border-box; font-size: 12.996px;"><em style="box-sizing: border-box;">MAKE MY TRIP HOTEL AGENT PANEL</em></strong><span style="font-size: 12.996px;">&nbsp;</span><strong style="box-sizing: border-box; font-size: 12.996px;">LOGIN</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Login using username and password as registered</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">CONTROL PANEL</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Agent Profile</li>
<li style="box-sizing: border-box;">Deposit</li>
<li style="box-sizing: border-box;">Target and Incentives would be maintained</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">H</strong><strong style="box-sizing: border-box; font-size: 12.996px;">OTEL MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Agent can add the hotel details.</li>
<li style="box-sizing: border-box;">He can view and block the room that’s not of interest.</li>
<li style="box-sizing: border-box;">Manage hotel details (edit/delete/block/unblock).</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">R</strong><strong style="box-sizing: border-box; font-size: 12.996px;">OOM MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">View the particular room count and details</li>
<li style="box-sizing: border-box;">View booker and travel details</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">BOOKING REPORT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Ledger details</li>
<li style="box-sizing: border-box;">Detailed report</li>
<li style="box-sizing: border-box;">Monthly-yearly-today chart would be maintained</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">REPORTS</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Transaction reports would be maintained</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">WALLET DEPOSIT/PRINT TICKET/CANCEL</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Amount in Wallet deposited could be reviewed</li>
<li style="box-sizing: border-box;">Can print or cancel ticket.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;"><em style="box-sizing: border-box;">MAKE MY TRIP HOTEL USER PANEL</em></strong><span style="font-size: 12.996px;">&nbsp;</span><strong style="box-sizing: border-box; font-size: 12.996px;">L</strong><strong style="box-sizing: border-box; font-size: 12.996px;">OGIN</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">User can login using Gmail or Facebook.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">R</strong><strong style="box-sizing: border-box; font-size: 12.996px;">EGISTER</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Basic account and contact information.</li>
<li style="box-sizing: border-box;">Once registered, a notification to be sent to the provided email.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">PAYMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Payment could be processed through preferred payment gateway.</li>
<li style="box-sizing: border-box;">Booking details would be generated through SMS.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">W</strong><strong style="box-sizing: border-box; font-size: 12.996px;">ALLET</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Registered users would be capable of accessing wallet, where the day’s offer price would be transferred automatically to the wallet, at the time of booking.</li>
<li style="box-sizing: border-box;">Guest users can as well avail the offer, but the difference is that as they don’t have access to a wallet, the offer price would be directly deducted from the booked price.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">P</strong><strong style="box-sizing: border-box; font-size: 12.996px;">RINT&nbsp; OR CANCEL</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">User can print/cancel his booking, and the refund would be processed either to the wallet or to his payment gateway account.</li>
</ul>
<strong style="box-sizing: border-box; font-size: 12.996px;">ROUTE MANAGEMENT</strong><span style="font-size: 12.996px; font-size: 12.996px;"></span><br />
<ul style="box-sizing: border-box; font-size: 12.996px; list-style: none; margin: 0px 0px 1em; padding: 0px;">
<li style="box-sizing: border-box;">Select the source city</li>
<li style="box-sizing: border-box;">Add the destination city</li>
</ul>
<div>
<span style="font-size: 12.996px;"><br /></span></div>
<div>
<span style="font-size: 12.996px;"><br /></span></div>
<div>
<span style="font-size: 12.996px;"><br /></span></div>
<div>
<span style="font-size: 12.996px;">Check Out Our Product in:</span></div>
<div>
<span style="font-size: 12.996px;"><br /></span></div>
<div>
<span id="docs-internal-guid-27acbd5f-a701-04b5-7813-ae28e9e601c3"></span><br />
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span id="docs-internal-guid-27acbd5f-a701-04b5-7813-ae28e9e601c3"><span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><a href="https://www.doditsolutions.com/makemytrip-clone/">https://www.doditsolutions.com/makemytrip-clone/</a></span></span></div>
<span id="docs-internal-guid-27acbd5f-a701-04b5-7813-ae28e9e601c3">
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><a href="http://scriptstore.in/product/makemytrip-clone/">http://scriptstore.in/product/makemytrip-clone/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><a href="http://phpreadymadescripts.com/make-my-trip-clone.html">http://phpreadymadescripts.com/make-my-trip-clone.html</a></span></div>
<div>
<span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><br /></span></div>
</span></div>
</div>
</form>
</div>
